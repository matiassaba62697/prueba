package ar.unju.edu.ar.prueba.dominio;

import org.apache.log4j.Logger;

public  class Estirables extends Programa{

	private static Logger log =Logger.getLogger(programacion.class);
	public Estirables(String nombre, int horaInicio, int horaFin, Programa programaQueLeSigue) {
		super(nombre, horaInicio, horaFin, programaQueLeSigue);
		// TODO Auto-generated constructor stub
	}

	public void estirar(int hs) {
		log.info("se va a estirar "+hs+" horas");
	setHoraFin(getHoraFin()+hs);
	for(Programa p:programaquelesiguen()) {
		p.retrasar(hs);
	}
	}


}
