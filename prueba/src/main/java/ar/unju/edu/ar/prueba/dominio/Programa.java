package ar.unju.edu.ar.prueba.dominio;

import java.util.ArrayList;
import org.apache.log4j.*;
import java.util.List;

import org.w3c.dom.views.AbstractView;
import org.w3c.dom.views.DocumentView;

public abstract class Programa {
	
private String nombre;
private int horaInicio;
private int horaFin;
private Programa sigue;


public void retrasar(int hs) {
	this.horaInicio+= hs;
	this.horaFin+=hs;
	for(Programa p:programaquelesiguen()) {
		p.retrasar(hs);	
	}
	
}
public List<Programa> programaquelesiguen(){
	List<Programa> lista=new ArrayList<Programa>();
	Programa siguente=sigue;
	while(null != siguente) {
		lista.add(siguente);
		siguente= siguente.sigue;
	}
	return lista;
}
 



public abstract void estirar(int hs);



public Programa(String nombre, int horaInicio, int horaFin, Programa programaQueLeSigue) {
	super();
	this.nombre = nombre;
	this.horaInicio = horaInicio;
	this.horaFin = horaFin;
	this.sigue = programaQueLeSigue;
}








@Override
public String toString() {
	return "Programa [nombre=" + nombre + ", horaInicio=" + horaInicio + ", horaFin=" + horaFin+"]";
}








public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public int getHoraInicio() {
	return horaInicio;
}
public void setHoraInicio(int horaInicio) {
	this.horaInicio = horaInicio;
}
public int getHoraFin() {
	return horaFin;
}
public void setHoraFin(int horaFin) {
	this.horaFin = horaFin;
}
public Programa getProgramaQueLeSigue() {
	return sigue;
}
public void setProgramaQueLeSigue(Programa programaQueLeSigue) {
	this.sigue = programaQueLeSigue;
}
public DocumentView getDocument() {
	// TODO Auto-generated method stub
	return null;
}






}
