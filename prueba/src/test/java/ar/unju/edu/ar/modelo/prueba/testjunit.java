package ar.unju.edu.ar.modelo.prueba;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.unju.edu.ar.prueba.dominio.Estirables;
import ar.unju.edu.ar.prueba.dominio.NoEstirables;
import ar.unju.edu.ar.prueba.dominio.Programa;
import ar.unju.edu.ar.prueba.dominio.programacion;
import ar.unju.edu.ar.prueba.exeptions.exeption;

public class testjunit {
programacion target;


	@Before
	public void setUp() throws Exception {
target = new programacion();
	}

	@After
	public void tearDown() throws Exception {
		target = null;
	}

	@Test
	public void test() throws exeption, Exception {
		
		Programa noticiero=new NoEstirables("noticiero", 16, 18, null);
		Programa partido=new Estirables("partido", 13, 16, noticiero);
		Programa novela=new NoEstirables("novela", 12, 13, partido);

		
		target.agregarProgramas(novela);
		target.agregarProgramas(partido);
		target.agregarProgramas(noticiero);
		
		List<Programa> listaProgramacion=target.listaProgramas(novela);
	
		for(Programa p:listaProgramacion) {
			System.out.println(p);
		}
		System.out.println("---------------------------------------------");
target.estirar(partido, 5);
for(Programa p:listaProgramacion) {
	System.out.println(p);
}
assertTrue(listaProgramacion.get(2).getHoraInicio()==21);
	}
	
	
	
	
	@Test
	public void testNoEstirabe() throws exeption, Exception {
		System.out.println("---------------------------------------------");
		Programa noticiero=new NoEstirables("noticiero", 16, 18, null);
		Programa partido=new Estirables("partido", 13, 16, noticiero);
		Programa novela=new NoEstirables("novela", 12, 13, partido);

		
		target.agregarProgramas(novela);
		target.agregarProgramas(partido);
		target.agregarProgramas(noticiero);
		
		List<Programa> listaProgramacion=target.listaProgramas(novela);
		for(Programa p:listaProgramacion) {
			System.out.println(p);
		}
		System.out.println("---------------------------------------------");
target.estirar(novela, 5);
for(Programa p:listaProgramacion) {
	System.out.println(p);
}
assertTrue(listaProgramacion.get(2).getHoraFin()==18);
	}

}
